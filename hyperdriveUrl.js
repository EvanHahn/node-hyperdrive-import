export function create({ key, discoveryKey, path }) {
  const result = new URL("hyper+node:");

  result.searchParams.set("key", key.toString("hex"));
  result.searchParams.set("discoverykey", discoveryKey.toString("hex"));
  result.searchParams.set("path", path);

  return result.href;
}

export function parse(href) {
  let url;
  try {
    url = new URL(href);
  } catch (_err) {
    return null;
  }

  // TODO: This function could be more robust.

  if (url.protocol !== "hyper+node:") return null;

  const rawKey = url.searchParams.get("key");
  const rawDiscoveryKey = url.searchParams.get("discoverykey");
  const path = url.searchParams.get("path");

  return {
    key: Buffer.from(rawKey, "hex"),
    discoveryKey: Buffer.from(rawDiscoveryKey, "hex"),
    path,
  };
}
