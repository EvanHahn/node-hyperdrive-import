import { describe, it, before, after, beforeEach, afterEach } from "node:test";
import * as assert from "node:assert/strict";
import { register, createRequire } from "node:module";
import Corestore from "corestore";
import Hyperswarm from "hyperswarm";
import Hyperdrive from "hyperdrive";
import ram from "random-access-memory";
import * as hyperdriveUrl from "./hyperdriveUrl.js";

const require = createRequire(import.meta.url);
const createTestnet = require("hyperdht/testnet");

describe("Hyperdrive import", () => {
  // Testnet

  let testnet;

  before(async () => {
    testnet = await createTestnet();
  });

  after(() => testnet.destroy());

  // Registration

  before(() => {
    register("./hooks.js", import.meta.url, {
      data: { bootstrap: testnet.bootstrap },
    });
  });

  // Writer

  let writerSwarm;
  let writerDrive;

  beforeEach(async () => {
    const { bootstrap } = testnet;

    const store = new Corestore(ram);

    writerSwarm = new Hyperswarm({ bootstrap });
    writerSwarm.on("connection", (conn) => {
      store.replicate(conn);
    });

    writerDrive = new Hyperdrive(store);
    await writerDrive.ready();

    const discovery = writerSwarm.join(writerDrive.discoveryKey, {
      client: false,
      server: true,
    });
    await discovery.flushed();
  });

  afterEach(() => writerSwarm.destroy());

  // Tests

  it("can import from a Hyperdrive", async () => {
    const { key, discoveryKey } = writerDrive;

    const modPromise = import(
      hyperdriveUrl.create({
        key,
        discoveryKey,
        path: "/foo.js",
      })
    );

    const source = "export function foo() { return 'bar' }";
    await writerDrive.put("/foo.js", Buffer.from(source, "utf8"));

    const mod = await modPromise;
    assert.equal(mod.foo(), "bar");
  });
});
