import { describe, it } from "node:test";
import * as assert from "node:assert/strict";
import { create, parse } from "./hyperdriveUrl.js";

describe("hyperdrive URL", () => {
  it("can create hrefs", () => {
    assert.equal(
      create({
        key: Buffer.from("abcd", "hex"),
        discoveryKey: Buffer.from("010203", "hex"),
        path: "/foo.js",
      }),
      "hyper+node:?key=abcd&discoverykey=010203&path=%2Ffoo.js",
    );
  });

  it("can parse hrefs", () => {
    assert.deepEqual(
      parse("hyper+node:?key=abcd&discoverykey=010203&path=/foo.js"),
      {
        key: Buffer.from([0xab, 0xcd]),
        discoveryKey: Buffer.from([0x01, 0x02, 0x03]),
        path: "/foo.js",
      },
    );
  });
});
