# Node Hyperdrive import

A proof-of-concept showing how to download Node modules over [Hyperdrive].

_This is not currently recommended for production use (see below)._

## Usage

This module has not been published, as it is just a proof-of-concept. You should be able to download and install it from your file system.

1. Install `hyperdrive-import`:

   ```sh
   npm install /path/to/node-hyperdrive-import
   ```

1. Create a file that imports from a file hosted in a Hyperdrive. You'll need the file's path, the drive's key, and the drive's discovery key (topic).

   ```javascript
   import { greet } from "hyper+node:?key=0e7fb02d2b34046b9e66d19fd4b1ac65c41bc4e7527a76ee1c4077c49bc57b97&discoverykey=97ea1a0179eb5eb25bf9abe1092916bdef0c8816c216f5b24bc10fbb5523a27b&path=%2Fgreet.js";
   console.log(greet());
   ```

   All files must use ES Modules. CommonJS is not supported (yet?).

1. Run Node with the `--import hyperdrive-import` flag added.

   ```sh
   node --import hyperdrive-import my-app.js
   # => Hello world!
   ```

See this repo's `example/` folder for a runnable demo.

## How it works

Node supports ["Customization Hooks"](https://nodejs.org/docs/latest-v21.x/api/module.html#customization-hooks), which let you customize how modules are imported. `hyperdrive-import` takes advantage of this feature.

Effectively, here's what this module does:

1. Adds an import hook that activates when importing a file using the `hyper+node` scheme.
1. When activated, downloads the file and returns it.

As of this writing, Customization Hooks are unstable in Node, but it's at the "release candidate" [stage](https://nodejs.org/docs/latest-v21.x/api/documentation.html#stability-index).

## See also

This module is very similar to [torrent-import](https://evanhahn.com/node-torrent-import/). Check that out for much more information.

[Hyperdrive]: https://docs.holepunch.to/building-blocks/hyperdrive
