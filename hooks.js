import { once } from "node:events";
import * as path from "node:path";
import { tmpdir } from "node:os";
import Corestore from "corestore";
import Hyperdrive from "hyperdrive";
import Hyperswarm from "hyperswarm";
import findCacheDirectory from "find-cache-dir";
import * as hyperdriveUrl from "./hyperdriveUrl.js";

let bootstrap;

const getCacheRoot = () =>
  findCacheDirectory({ name: "hypercore-import" }) ??
  path.join(tmpdir(), "hypercore-import");

async function download({ key, discoveryKey, path: hyperdrivePath }) {
  const cacheDir = path.join(getCacheRoot(), key.toString("hex"));
  const store = new Corestore(cacheDir);

  const swarm = new Hyperswarm({ bootstrap });
  swarm.on("connection", (conn) => {
    store.replicate(conn);
  });

  const drive = new Hyperdrive(store, key);
  const appendPromise = once(drive.core, "append");
  await drive.ready();

  const foundPeers = store.findingPeers();
  const discovery = swarm.join(discoveryKey, { client: true, server: false });

  await appendPromise;
  await discovery.flushed();
  await swarm.flush();
  foundPeers();

  const result = await drive.get(hyperdrivePath);

  await swarm.destroy();

  return result;
}

export function initialize(data) {
  if (data && typeof data === "object" && data.bootstrap) {
    ({ bootstrap } = data);
  }
}

export async function load(url, context, nextLoad) {
  const params = hyperdriveUrl.parse(url);
  if (!params) return nextLoad(url, context);

  return {
    format: "module",
    shortCircuit: true,
    source: await download(params),
  };
}
