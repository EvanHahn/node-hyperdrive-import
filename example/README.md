# Node Hyperdrive import example

To run this example:

1. Install dependencies:

   ```sh
   npm install
   ```

1. Start the writer. Leave this running for the rest of the example, as it "hosts" the file.

   ```sh
   npm run writer
   ```

   It should output a code snippet like `import { greet } from "hyper+node:?key=abc123...`. Copy this line.

1. Create `main.js` using the line from the previous step. Refer to `main.js.sample` for a sample.

1. Run the import:

   ```sh
   npm start
   ```
