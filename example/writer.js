import Hyperswarm from "hyperswarm";
import Hyperdrive from "hyperdrive";
import Localdrive from "localdrive";
import Corestore from "corestore";
import goodbye from "graceful-goodbye";
import * as hyperdriveImportUrl from "hyperdrive-import/url";

const store = new Corestore(
  "/tmp/node-hyperdrive-import-example-writer-storage",
);
const swarm = new Hyperswarm();
goodbye(() => swarm.destroy());

swarm.on("connection", (conn) => store.replicate(conn));

const local = new Localdrive("./writer-dir");

const drive = new Hyperdrive(store);
await drive.ready();

const discovery = swarm.join(drive.discoveryKey);
await discovery.flushed();

const mirror = local.mirror(drive);
await mirror.done();

const importUrl = hyperdriveImportUrl.create({
  key: drive.key,
  discoveryKey: drive.discoveryKey,
  path: "/greet.js",
});
console.log("Hosting drive. Try importing greet.js with:");
console.log();
console.log(`import { greet } from "${importUrl}";`);
